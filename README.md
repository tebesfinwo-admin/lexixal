# README #

### What is this repository for? ###

A Lexical Analyzer for a simple programming language

### How do I get set up? ###

To Compile the Code :
```
$ javac src/*.java
```

To Run :
```
$ java MicroPL86 <fileName>
```

### Who do I talk to? ###

[@tebesfinwo-admin](https://bitbucket.org/tebesfinwo-admin/)