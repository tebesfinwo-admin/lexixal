import java.io.File;
import java.io.IOException;

/**
 * Created by tebesfinwo on 11/23/14.
 */
public class MicroPL86 {

    static String fileName = null;

    public static void main(String[] args) throws IOException {

        processCommandLine(args);

        if (fileName == null) {
            System.out.println("Usage: MicroPL86 <filename>");
            System.exit(2);
        }

        Lexer lexer = new Lexer(new File(fileName));

        while(lexer.token().type != Token.EOI) {
            System.out.println(lexer.token());
            lexer.next();
        }


    }

    static void processCommandLine(String[] args){
        for(String arg : args){
            if ( arg.startsWith("-") ) {

            } else {
                fileName = arg;
            }
        }
    }

}
